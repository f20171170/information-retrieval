import random
import functools
import numpy as np
from math import sqrt
from typing import List

NUMBER_OF_USERS = 100  # Y Axis
NUMBER_OF_ITEMS = 1000  # X Axis


class UserBasedCollaborativeRecommender:
    def __init__(self, ratings_matrix: np.ndarray=np.zeros((1,1))) -> None:
        global NUMBER_OF_USERS, NUMBER_OF_ITEMS
        if ratings_matrix.shape == (1, 1):
            # Initialize the ratings_matrix and have every user rate every item.
            self.ratings_matrix = np.zeros((NUMBER_OF_USERS, NUMBER_OF_ITEMS), dtype="int")
            for cnt_i in range(0, NUMBER_OF_USERS):
                for cnt_j in range(0, NUMBER_OF_ITEMS):
                    # NOTE: One drawback of using completely random values for this
                    # example is that it's not realistic nor does it align with what
                    # the model is supposed to be doing. E.g. if items 1 and 2 were
                    # similar items, then we would expect users to rate the two
                    # similarly. In other words, using completely random values would
                    # destroy the concept of similarity which is what this model
                    # is actually based on. This example merely provides the code, but
                    # to make the results more realistic, we would need to make the
                    # example data more realistic.
                    # Though not done in this step, we will assume that a value of 0
                    # means un-rated.
                    self.ratings_matrix[cnt_i][cnt_j] = random.randint(1, 5)
        else:
            NUMBER_OF_USERS, NUMBER_OF_ITEMS = ratings_matrix.shape
            self.ratings_matrix = ratings_matrix

    @functools.lru_cache(maxsize=2)
    def getMean(self, user: int) -> float:
        # Get the average rating of the user. We use this to remove any bias.
        total = 0
        number_of_items = 0
        for item in range(NUMBER_OF_ITEMS):
            if self.ratings_matrix[user][item] == 0:
                # Then the item was not rated and should not be counted.
                continue
            total += self.ratings_matrix[user][item]
            number_of_items += 1
        if number_of_items == 0:
            raise ValueError("User %d has not rated anything." % user)
        mean = total/number_of_items
        return mean

    def getCommonItems(self, user_a: int, user_b: int) -> List[int]:
        # Return a list of items that both users have rated.
        common_items = [item for item in range(NUMBER_OF_ITEMS) if
                        self.ratings_matrix[user_a][item] != 0 and
                        self.ratings_matrix[user_b][item] != 0]
        return common_items

    def getSimilarity(self, user_a: int, user_b: int) -> float:
        # Use Pearson's coefficient to determine how similar the users' ratings
        # vectors are.
        mean_a = self.getMean(user_a)
        mean_b = self.getMean(user_b)
        common_items = self.getCommonItems(user_a, user_b)  # type: List[int]
        x = 0
        y = 0
        z = 0
        for item in common_items:
            t1 = (self.ratings_matrix[user_a][item] - mean_a)
            t2 = (self.ratings_matrix[user_b][item] - mean_b)
            x += t1 * t2
            y += t1 ** 2
            z += t2 ** 2
        sim = x / (sqrt(y) * sqrt(z))
        return sim


    def getPredictedRating(self, target_user: int, item: int) -> float:
        num = 0
        den = 0
        for user in range(NUMBER_OF_USERS):
            if user == target_user:
                continue
            rating = self.ratings_matrix[user][item]
            if rating == 0:
                # Then this user should not be considered in the list as they
                # have not yet rated this item.
                continue
            mean_b = self.getMean(user)
            sim = self.getSimilarity(target_user, user)
            num += sim * (rating - mean_b)
            den += abs(sim)
        mean_a = self.getMean(target_user)
        predicted_rating = mean_a + (num / den)
        return predicted_rating


if __name__ == "__main__":
    # Now we will pretend like the first user didn't rate the first item (but has
    # rated every other item) then we will predict the value of the rating for the
    # first item.
    recommender = UserBasedCollaborativeRecommender()
    recommender.ratings_matrix[0][0] = 0
    print(recommender.ratings_matrix)
    prediction = recommender.getPredictedRating(0, 0)
    print(prediction)
    assert prediction > 0 and prediction <= 5


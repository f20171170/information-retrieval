#include "boolean_query.h"

Posting intersect_postings(Posting p1, Posting p2) {
    Posting solution;
    Posting::iterator i = p1.begin();
    Posting::iterator j = p2.begin();
    while(i != p1.end() and j != p2.end()) {
        if (*i == *j) {
            solution.push_back(*i);
            i = next(i);
            j = next(j);
        }
        else {
            if (*i < *j)
                i = next(i);
            else
                j = next(j);
        }
    }
    return solution;
}


vector<string> sort_terms_by_increasing_frequency(vector<string> terms, InvertedIndex index) {
    vector<int> frequency;
    for(string term: terms) {
        frequency.push_back(index.at(term).size());
    }
    // we will sort with a simple insertion sort.
    for(int i=1; i<frequency.size(); ++i) {
        for(int j=i; j>0; --j) {
            if(frequency[j-1] > frequency[j]) {
                int temp_f;
                temp_f = frequency[j-1];
                frequency[j-1] = frequency[j];
                frequency[j] = temp_f;
                string temp_term;
                temp_term = terms[j-1];
                terms[j-1] = terms[j];
                terms[j] = temp_term;
                continue;
            }
            break;
        }
    }
    return terms;
}


Posting conjugative_boolean_query(vector<string> terms, InvertedIndex index) {
    terms = sort_terms_by_increasing_frequency(terms, index);
    vector<string>::iterator term = terms.begin();
    Posting result = index[*term];
    term = next(term);
    while (not(term == terms.end())) {
        result = intersect_postings(result, index[*term]);
        term = next(term);
    }
    return result;
}

Posting disjunctive_boolean_query(vector<string> terms, InvertedIndex index) {
    terms = sort_terms_by_increasing_frequency(terms, index);
    vector<string>::iterator term = terms.begin();
    Posting result = index[*term];
    term = next(term);
    while (not(term == terms.end())) {
        result = intersect_postings(result, index[*term]);
        term = next(term);
    }
    return result;
}
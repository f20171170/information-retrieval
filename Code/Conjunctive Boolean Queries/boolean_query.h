#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
using namespace std;

typedef vector<int> Posting;
typedef unordered_map<string, Posting> InvertedIndex;

Posting conjugative_boolean_query(vector<string> terms, InvertedIndex index);
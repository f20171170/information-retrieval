#include "boolean_query.h"

int main(int argc, char** argv) {
    InvertedIndex index;
    index["term3"] = {1, 2, 3, 4, 5};
    index["term2"] = {1, 4, 5, 6, 7, 8};
    index["term1"] = {1, 2};

    vector<string> terms = {"term1", "term2", "term3"};

    Posting result = conjugative_boolean_query(terms, index);
    for(int i: result)
        cout << i << " ";
    cout << endl;
    return 0;
}

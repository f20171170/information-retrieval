from naive_bayes import NaiveBayesClassifier

training_data = [
    "Kyoto is the former capital of Japan",
    "Bejing is the current capital of China",
    "New Delhi is the current capital of India",
    "Tokyo is the Japanese capital",
    "Prime minister Narendra Modi hangs out with Bear Grylls",
    "Wen Jiabao was a former Premier of China",
    "Shinzo Abe is the Japanese prime minister",
    "Hyderabad is becoming a global tech hub"
]

labels = [
    "Japan",
    "China",
    "India",
    "Japan",
    "India",
    "China",
    "Japan",
    "India"
]

testing_data = [
    "Premier Wen Jiabao visits Kyoto Japan",
    "Kyoto Animations opens new headquaters in Tokyo",
    "Bear Grylls looks to go hiking with Shinzo Abe",
    "Shinzo Abe visits New Delhi to meet with Narendra Modi",
    "Narendra Modi goes to Tokyo to talk with Shinzo Abe"
]

classifier = NaiveBayesClassifier()
classifier.train(training_data, labels)

for test_document in testing_data:
    print(test_document)
    print("Result: {}\n".format(classifier.classify(test_document, verbose=True), test_document))
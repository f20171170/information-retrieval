#!/usr/bin/env python3
from math import log
from typing import Dict, List, Tuple
from collections import defaultdict

class NaiveBayesClassifier(object):
    def __init__(self) -> None:
        self.categorial_probabilities = defaultdict(int)  # Dict[str, float]
        self.conditional_probabilities = defaultdict(lambda: defaultdict(float))  # Dict[str, Dict[str, float]]
        self.number_of_tokens_by_category = defaultdict(int)  # Dict[str, int]

    def normalize_token(self, token: str) -> str:
        # We can apply normalization/stemming/lemmatization here.
        # We can also kill off stop words at this point.
        # However, here, we aren't doing much (just blocking a
        # few stop words). Words like "San Francisco" would count
        # as two separate words instead of just one.
        if token in ["is", "the", "of", "in", "was", "a", "to", "with"]:
            return ""
        return token.upper()

    def __preprocess_data(self, documents: List[str], labels: List[str]) -> Tuple[Dict[str, Dict[str, int]],
                                                                                  Dict[str, int],
                                                                                  Dict[str, int]]:
        if len(documents) != len(labels):
            raise AssertionError("Number of documents and labels do not match.")

        vocabulary = defaultdict(lambda: defaultdict(int))  # Dict[str, Dict[str, int]]
        number_of_tokens_by_category = defaultdict(int)  # Dict[str, int]
        category_document_count = defaultdict(int)  # Dict[str, int]

        for category, document in zip(labels, documents):
            category_document_count[category] += 1
            for token in document.strip().split(" "):
                token = self.normalize_token(token)
                if token == "":
                    continue
                vocabulary[token][category] += 1
                number_of_tokens_by_category[category] += 1

        return (vocabulary, number_of_tokens_by_category, category_document_count)

    def train(self, documents: List[str], labels: List[str]) -> None:
        vocabulary, number_of_tokens_by_category, category_document_count = self.__preprocess_data(documents, labels)
        number_of_documents = len(documents)
        number_of_terms = len(vocabulary.keys())

        self.number_of_tokens_by_category = number_of_tokens_by_category

        for category, document_count in category_document_count.items():
            self.categorial_probabilities[category] = document_count/number_of_documents

        for term in vocabulary.keys():
            for category in category_document_count.keys():
                # Apply Laplace smoothing when calculating the conditional_probabilities.
                numerator = vocabulary[term].get(category, 0) + 1
                denomenator = number_of_tokens_by_category[category] + number_of_terms
                cp = numerator/denomenator
                self.conditional_probabilities[term][category] = cp

    def classify(self, document: str, verbose: bool=False) -> str:
        maximum_probablity = float("-inf")
        maximum_probablity_category = ""
        vocabulary_size = len(self.conditional_probabilities.keys())

        for category, categorial_probability in self.categorial_probabilities.items():
            categorial_probability = log(categorial_probability, 10)
            terms_probablity = 0
            for token in document.strip().split(" "):
                token = self.normalize_token(token)
                if token == "":
                    continue
                conditional_probability = self.conditional_probabilities[token][category]
                if conditional_probability == 0:
                    # Then we're encountering a new term that does not exist in the vocabulary.
                    conditional_probability = 1/(self.number_of_tokens_by_category[category] + vocabulary_size)
                terms_probablity += log(conditional_probability, 10)
            net_probabilty = categorial_probability + terms_probablity
            if verbose:
                print("    {}: {} [{}, {}]".format(category, categorial_probability + terms_probablity, categorial_probability, terms_probablity))
            if net_probabilty > maximum_probablity:
                maximum_probablity = net_probabilty
                maximum_probablity_category = category

        return maximum_probablity_category

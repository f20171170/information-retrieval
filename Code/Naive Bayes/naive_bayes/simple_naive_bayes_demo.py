from naive_bayes import NaiveBayesClassifier

training_data = [
    "Chinese Bejing Chinese",
    "Chinese Chinese Shanghai",
    "Chinese Macao",
    "Tokyo Japan Chinese"
]

labels = [
    "China",
    "China",
    "China",
    "Not China"
]

testing_data = [
    "Chinese Chinese Chinese Tokyo Japan",
    "Comet hits Itomori"  # Marked as China because of the categorial_probabilities
]

classifier = NaiveBayesClassifier()
classifier.train(training_data, labels)

for test_document in testing_data:
    print(test_document)
    print("Result: {}\n".format(classifier.classify(test_document, verbose=True), test_document))
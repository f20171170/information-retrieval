# Naive Bayes for Document Classification
This repository contains a sample implementation of Naive Bayes for document
classification. This implementation is based on the teaching from the
textbook "Introduction to Information Retrieval" by the Stanford NLP Group
(https://nlp.stanford.edu/IR-book/information-retrieval-book.html).  

This repository also provides to samples of training and testing using the
code in `naive_bayes.py`.  

Some Notes:

- This implementation is not a hard and fast implementation of the high level
algorithm given in the textbook but it is quite close and has the same time
complexities for preprocessing + training and testing.

- This implementation can be improved by implementing more serious token
normalization (e.g. Porter's algorithm for stemming and a more comprehensive
list of stop words).



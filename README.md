# Information Retrieval - CS F469 at BITS Pilani
This is a repository where I will store implementations of a few select
algorithms and techinques taught during CS F469 (2019-20 Fall Semester).

All code wil be available in the "Code" directory and some learning
resources for the course will be available in the "Resources" directory.
